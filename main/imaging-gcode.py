from time import strftime
print("\n")
print("CREATION OF .GCODE AND POSITION .CSV FILES")
print("\n")
maxx=float(input("Please enter the sampling area in x direction [mm]: "))
maxy=float(input("Please enter the sampling area in y direction [mm]: "))
resolution=float(input("Please enter the desired resolution [mm]: "))
timing=float(input("Please enter the waiting time on each point [s]: "))

maxxrange=int(maxx/resolution)
maxxcounter=int(maxxrange+1)
maxyrange=int(maxy/resolution)
maxycounter=int(maxyrange+1)


gfilename=strftime("%Y%m%d_%H%M%S") + ".gcode"
posfilename=strftime("%Y%m%d_%H%M%S") + ".csv"

gfile = open(gfilename, "a")
posfile = open(posfilename, "a")

gfile.write("G21\n")
gfile.write("G91\n")
movedir=1
gfile.write("G4 S%f\n" % timing)
for y in range(0, maxycounter):
	for x in range(0,maxxcounter):
		coordx=str((x) * resolution)
		coordy=str((y) * resolution)
		posfile.write(coordx)
		posfile.write(",")
		posfile.write(coordy)
		posfile.write("\n")		
		if x==maxxrange and y<maxyrange:
			gfile.write("G0 Y%f\n" % resolution)
			gfile.write("G4 S%f\n" % timing)
			movedir=-1*movedir
		elif x==maxxrange and y==maxyrange:
			print("\n")	
		elif movedir>0:
			gfile.write("G0 X%f\n" % resolution)
			gfile.write("G4 S%f\n" % timing)
		else:
			gfile.write("G0 X-%f\n" % resolution)
			gfile.write("G4 S%f\n" % timing)
gfile.close()
posfile.close()
print("\n")
print(gfilename,"and",posfilename,"file generated!\n")
samplingtime=maxxcounter*maxycounter*timing
print("Approximate sampling time:",samplingtime,"seconds")
print("\n")


