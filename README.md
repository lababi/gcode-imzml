# README #

imaging-gcode.py: Creates a .gcode path for sampling/ imaging with a RepRap system. The program depends on Python v3 and is invoked by "#python3 imaging-gcode.py". Two files a generated: a .gcode file with the G code for moving the robot, and a .csv file with the comma separated values describing the x, y coordinates of each sampling point. 

create_imzML.R: This R script combines a x, y .csv file and a .mzML spectra file to an .imzML mass spectrometry imaging (MSI) archive.

# CONTACT #

Dr. Robert Winkler

robert.winkler@bioprocess.org
robert.winkler@ira.cinvestav.mx